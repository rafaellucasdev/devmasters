import React, { useState, useEffect } from "react";
import "./App.css";

import NavBar from "./components/NavBar";
import SideBar from "./components/SideBar";
import Footer from "./components/Footer";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages";
import Sobre from "./pages/sobre";
// import Contato from "./pages/contato";
import Blog from "./pages/blog";
import Servicos from "./pages/servicos";


const App = () => {
  const [ setShowLoader] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setShowLoader(false);
    }, 1000);
  });

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <>
      <Router>
        <SideBar isOpen={isOpen} toggle={toggle} />
        <NavBar toggle={toggle} />
        <Routes>
          <Route path="/" exac element={<Home />} />
          <Route path="/sobre" element={<Sobre />} />
          {/* <Route path="/contato" element={<Contato />} /> */}
          <Route path="/blog" element={<Blog />} />
          <Route path="/servicos" element={<Servicos />} />
          {/* <Route
            path="/desenvolvimento-de-software"
            element={<DesenvolvimentoDeSoftware />}
          /> */}
        </Routes>
        <Footer />
      </Router>
    </>
  );
};

export default App;
