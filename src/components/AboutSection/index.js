import React from "react";
import {
  AboutContainer,
  AboutSectionWrapper,
  AboutH1,
  AboutP,
} from "./components";
// import imageBackgound from "./../../images/image01.jpg";

const AboutSection = () => {
  return (
    <AboutContainer id="IdSobre">
      <AboutSectionWrapper>
        <AboutH1>Sobre nós</AboutH1>
        <AboutP>
          Temos total foco no seu negócio. Nascemos da ideia de conectar pessoas
          e suas conquistas. Enxergamos que para ter sucesso é necessário muito
          trabalho e conhecimento. Somos a fusão de esforço, estratégia,
          comprometimento e resiliência. Nosso time é composto de especialistas
          em tecnologia que estarão a disposição para alavancar a sua ideia,
          afinal, aqui ela acontece!
        </AboutP>
      </AboutSectionWrapper>
    </AboutContainer>
  );
};

export default AboutSection;
