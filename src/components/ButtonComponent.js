import styled from "styled-components";
// import { Link } from "react-scroll";
import { Link as LinkR } from "react-router-dom";
import { Link as LinkS } from "react-scroll";

export const Button = styled(LinkR)`
  font-family: "Ubuntu", sans-serif;
  border-radius: 50px;
  background: ${({ primary }) => (primary ? "#e53944" : "#fff")};
  white-space: nowrap;
  padding: ${({ big }) => (big ? "14px 48px" : "12px 30px")};
  color: ${({ dark }) => (dark ? "#fff" : "#000")};
  font-size: ${({ fontBig }) => (fontBig ? "20px" : "16px")};
  outline: none;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.2s ease-in-out;
  margin: 15px;
  animation-name: fade;
  animation-duration: 800ms;
  text-decoration: none;

  @keyframes fade {
    from {
      opacity: 0;
      transform: scale(0.9);
    }
    to {
      opacity: 1;
      transform: scale(1);
    }
  }

  &:hover {
    transition: all 0.2s ease-in-out;
    background: ${({ primary }) => (primary ? "#DCDCDC" : "#256ce1")};
    color: ${({ dark }) => (dark ? "#000" : "#fff")};
  }
`;

export const ButtonS = styled(LinkS)`
  font-family: "Ubuntu", sans-serif;
  border-radius: 50px;
  background: ${({ primary }) => (primary ? "#e53944" : "#fff")};
  white-space: nowrap;
  padding: ${({ big }) => (big ? "14px 48px" : "12px 30px")};
  color: ${({ dark }) => (dark ? "#fff" : "#000")};
  font-size: ${({ fontBig }) => (fontBig ? "20px" : "16px")};
  outline: none;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.2s ease-in-out;
  margin: 15px;
  animation-name: fade;
  animation-duration: 800ms;
  text-decoration: none;

  @keyframes fade {
    from {
      opacity: 0;
      transform: scale(0.9);
    }
    to {
      opacity: 1;
      transform: scale(1);
    }
  }

  &:hover {
    transition: all 0.2s ease-in-out;
    background: ${({ primary }) => (primary ? "#DCDCDC" : "#256ce1")};
    color: ${({ dark }) => (dark ? "#000" : "#fff")};
  }
`;

// background: #256ce1;
