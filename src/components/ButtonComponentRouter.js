import styled from "styled-components";
import { Link } from "react-router-dom";


export const Button = styled(Link)`
  font-family: "Ubuntu", sans-serif;
  border-radius: 50px;
  background: ${({ primary }) => (primary ? "#e53944" : "#fff")};
  white-space: nowrap;
  padding: ${({ big }) => (big ? "14px 48px" : "12px 30px")};
  color: ${({ dark }) => (dark ? "#fff" : "#000")};
  font-size: ${({ fontBig }) => (fontBig ? "20px" : "16px")};
  outline: none;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.2s ease-in-out;
  margin: 15px;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: ${({ primary }) => (primary ? "#DCDCDC" : "#256ce1")};
    color: ${({ dark }) => (dark ? "#000" : "#fff")};
  }
`;

export const ButtonA = styled.a`
  font-family: "Ubuntu", sans-serif;
  border-radius: 50px;
  background: ${({ primary }) => (primary ? "#e53944" : "#e53944")};
  white-space: nowrap;
  padding: ${({ big }) => (big ? "14px 48px" : "12px 30px")};
  color: ${({ dark }) => (dark ? "#fff" : "#fff")};
  font-size: ${({ fontBig }) => (fontBig ? "20px" : "16px")};
  outline: none;
  border: none;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.2s ease-in-out;
  margin: 15px;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: ${({ primary }) => (primary ? "#DCDCDC" : "#DCDCDC")};
    color: ${({ dark }) => (dark ? "#000" : "#000")};
  }
`;

export const NavBtnLink = styled(Link)`
  font-family: "Exo 2", sans-serif;
  border-radius: 50px;
  display: flex;
  justify-content: center;
  background: #e53944;
  padding: 10px 22px;
  color: #fff;
  border: none;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: #fff;
    color: #010606;
  }
`;
// background: #256ce1;
