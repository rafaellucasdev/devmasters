import React from "react";
import {
  FaFacebook,
  FaInstagram,
  FaLinkedin,
  FaGithub,
  FaYoutube,
} from "react-icons/fa";
import {
  FooterContainer,
  FooterWrap,
  FooterLinksContainer,
  FooterLinksWrapper,
  FooterLinkItems,
  FooterLinkTitle,
  FooterLink,
  SocialMedia,
  SocialMediaWrap,
  SocialLogo,
  WebSiteRights,
  SocialIcons,
  SocialIconLink,
} from "./components";

const Footer = () => {
  return (
    <FooterContainer>
      <FooterWrap>
        <FooterLinksContainer>
          <FooterLinksWrapper>
            <FooterLinkItems>
              <FooterLinkTitle>Empresa</FooterLinkTitle>
              <FooterLink to="IdSobre">Sobre nós</FooterLink>
              <FooterLink to="contato">Contato</FooterLink>
              {/* <FooterLink to="/">Parcerias</FooterLink> */}
            </FooterLinkItems>
            <FooterLinkItems>
              <FooterLinkTitle>Serviços</FooterLinkTitle>
              <FooterLink to="/home">Desenvolvimento de software</FooterLink>
              <FooterLink to="/home">Marketing e Social Media</FooterLink>
              <FooterLink to="/home">Criação de sites</FooterLink>
              <FooterLink to="/home">Consultoria</FooterLink>
            </FooterLinkItems>
          </FooterLinksWrapper>
          <FooterLinksWrapper>
            {/* <FooterLinkItems>
              <FooterLinkTitle>Mídia</FooterLinkTitle>
              <FooterLink to="/home">Newsletter</FooterLink>
            </FooterLinkItems> */}
            <FooterLinkItems>
              <FooterLinkTitle>Parceria</FooterLinkTitle>
              <FooterLink to="contato">Agende uma reunião</FooterLink>
              <FooterLink to="/etapas">
                Pricipais etapas do nosso trabalho
              </FooterLink>
            </FooterLinkItems>
          </FooterLinksWrapper>
        </FooterLinksContainer>
        <SocialMedia>
          <SocialMediaWrap>
            <SocialLogo
              to="/"
              src={require("../../assets/img/logo.png")}
              alt="logo"
            />

            <WebSiteRights>
              Copyright &copy; {new Date().getFullYear()} todos os direitos
              reservados
            </WebSiteRights>
            <SocialIcons>
              <SocialIconLink href="https://www.facebook.com/profile.php?id=100081309828992" target="_blank" aria-label="Facebook">
                <FaFacebook />
              </SocialIconLink>
              <SocialIconLink href="https://www.instagram.com/dev.masters/" target="_blank" aria-label="Instagram">
                <FaInstagram />
              </SocialIconLink>
              <SocialIconLink href="https://www.linkedin.com/company/devmasters/" target="_blank" aria-label="Linkedin">
                <FaLinkedin />
              </SocialIconLink>
              {/* <SocialIconLink href="/" target="_blank" aria-label="Github">
                <FaGithub />
              </SocialIconLink> */}
              <SocialIconLink href="/" target="_blank" aria-label="Youtube">
                <FaYoutube />
              </SocialIconLink>
            </SocialIcons>
          </SocialMediaWrap>
        </SocialMedia>
      </FooterWrap>
    </FooterContainer>
  );
};

export default Footer;
