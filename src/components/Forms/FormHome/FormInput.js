import React, { useState } from "react";
import { InputContainer, Label } from "./components";
import "./style.css";

const FormInput = (props) => {
  const [focused, setFocused] = useState(false);
  const { label, errorMessage, onChange, id, ...inputProps } = props;

  const handleFocus = (e) => {
    setFocused(true);
  };
  return (
    <InputContainer>
      <Label>{label}</Label>
      <input
        className="Input"
        {...inputProps}
        onChange={onChange}
        onBlur={handleFocus}
        onFocus={() =>
          inputProps.name === "confirmPassword" && setFocused(true)
        }
        focused={focused.toString()}
      ></input>
      <span>{errorMessage}</span>
    </InputContainer>
  );
};

export default FormInput;
