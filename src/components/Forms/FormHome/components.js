import styled from "styled-components";

export const FormContainer = styled.div`
  background: #ececec;
`;

export const FormWrapper = styled.div`
  background: #ececec;
  display: grid;
  grid-auto-columns: minmax(auto, 1fr);
  align-items: center;
  z-index: 1;
  height: 800px;
  width: 100%;
  max-width: 1100px;
  margin-right: auto;
  margin-left: auto;
  padding: 0 24px;
  justify-content: center;
  grid-template-areas: ${({ imgStart }) =>
    imgStart ? `'col2 col1'` : `'col1 col2'`};

  @media screen and (max-width: 768px) {
    grid-template-areas: ${({ imgStart }) =>
      imgStart ? `'col1' 'col2'` : `'col1 col1' 'col2 col2'`};
  }

  @media screen and (max-width: 768px) {
    flex-direction: column;
    height: 1250px;
  }

  @media screen and (max-width: 480px) {
    flex-direction: column;
    height: 1000px;
  }
`;

export const FormCard = styled.form`
  background: #031d44;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  /* grid-gap: 16px; */
  padding: 30px 25px;
  border-radius: 10px;
  max-width: 500px;

  @media screen and (max-width: 768px) {
    padding: 20px 20px;
  }
`;

export const InputContainer = styled.div`
  align-items: left;
  justify-content: center;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const Label = styled.label`
  font-family: "Ubuntu", sans-serif;
  font-size: 14px;
  color: #fff;
`;

export const LabelTextArea = styled.textarea`
  font-size: 14px;
  color: #031d44;
  height: 50px;
`;

export const FormTitle = styled.h1`
  color: #fff;
  font-family: "Ubuntu", sans-serif;
  text-align: center;
  font-size: 28px;
  line-height: 1.1;
  font-weight: 600;
`;

export const ImgWrap = styled.div`
  max-width: 555px;
  height: 100%;
`;

export const Img = styled.img`
  width: 100%;
  margin: 0 0 10px 0;
  padding-right: 0;
`;

// export const Input = styled.input`
//   padding: 15px;
//   border-radius: 5px;
//   border: 1px solid gray;
//   width: 100%;

//   &:invalid[focused="true"] {
//     border: 1px solid red;
//   }

//   &:invalid[focused="true"] ~ Error {
//     display: block;
//   }
// `;

export const FormButton = styled.button`
  width: 100%;
  height: 50px;
  padding: 10px;
  border: none;
  background: #e53944;
  color: white;
  border-radius: 5px;
  font-weight: bold;
  font-size: 18px;
  cursor: pointer;
  margin-top: 15px;
  margin-bottom: 30px;

  &:hover {
    background: #ececec;
    color: #031d44;
    transition: all 0.2s ease-in-out;
  }
`;

export const Error = styled.span`
  font-size: 12px;
  padding: 3px;
  color: red;
  display: none;
`;

export const Column1 = styled.div`
  margin-bottom: 15px;
  padding: 0 15px;
  grid-area: col1;
`;

export const Column2 = styled.div`
  margin-bottom: 15px;
  padding: 0 15px;
  grid-area: col2;
`;

export const FormInfo = styled.div`
  max-width: 500px;
  align-items: center;
`;

export const InfoTitle = styled.h1`
  font-family: "Ubuntu", sans-serif;
  text-align: center;
  color: #031d44 !important;
  margin-bottom: 24px;
  font-size: 48px;
  line-height: 1.1;
  font-weight: 600;

  color: ${({ lightText }) => (lightText ? "#fff" : "#010606")};

  @media screen and (max-width: 480px) {
    font-size: 32px;
    text-align: center;
  }

  @media screen and (max-width: 768px) {
    font-size: 32px;
    text-align: center;
  }
`;

export const InfoDescription = styled.p`
  display: flex;
  align-items: center;
  color: #031d44 !important;
  font-weight: 600;
  max-width: 440px;
  margin-bottom: 35px;
  font-size: 18px;
  line-height: 24px;

  color: ${({ darkText }) => (darkText ? "#010606" : "#000")};

  @media screen and (max-width: 480px) {
    justify-content: center;
  }

  @media screen and (max-width: 768px) {
    justify-content: center;
  }
`;
