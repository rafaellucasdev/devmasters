import React, { useState } from "react";
import {
  FormContainer,
  FormWrapper,
  Column1,
  Column2,
  FormInfo,
  Img,
  ImgWrap,
  FormCard,
  FormButton,
  FormTitle,
} from "./components";
import FormInput from "./FormInput";
import img from "../../../images/09.png";
const FormHome = () => {
  const [values, setValues] = useState({
    name: "",
    phone: "",
    email: "",
    menssage: "",
  });

  const inputs = [
    {
      id: 1,
      name: "name",
      type: "text",
      placeholder: "Seu primeiro nome",
      errorMessage:
        "O nome deve acima de 2 caracteres e não deve incluir nenhum caractere especial!",
      label: "Nome",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 2,
      name: "email",
      type: "email",
      placeholder: "nome@exemplo.com",
      errorMessage: "Você deve digitar um email válido!",
      label: "Email",
      required: true,
    },
    {
      id: 3,
      name: "phone",
      type: "number",
      placeholder: "Telefone (com DDD)",
      label: "Telefone",
      errorMessage: "Você deve digitar um telefone válido!",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 4,
      name: "menssage",
      type: "textarea",
      placeholder: "Deixe sua mensagem",
      errorMessage:
        "Você deve enviar ao menos uma mensagem para que possamos entrar em contato",
      label: "Mensagem",
      pattern: `^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$`,
      required: false,
    },
  ];

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  return (
    <FormContainer >
      {/* <InfoTitle>Se preferir, a gente entra em contato com você!</InfoTitle> */}
      <FormWrapper id="contato" >
        <Column1>
          <ImgWrap>
            <Img src={img} />
          </ImgWrap>
          <FormInfo></FormInfo>
        </Column1>
        <Column2 >
          <FormCard onSubmit={handleSubmit}>
            <FormTitle>Te retornamos em no máximo duas horas!</FormTitle>

            {inputs.map((input) => (
              <FormInput
                key={input.id}
                {...input}
                value={values[inputs.name]}
                onChange={onChange}
              />
            ))}
            <FormButton>Enviar</FormButton>
          </FormCard>
          {/* <InfoDescription>
            {<IconMail />}contato@devmasters.dev.br{" "}
          </InfoDescription>
          <InfoDescription>{<IconFone />}(61) 98951-5010 </InfoDescription> */}
        </Column2>
      </FormWrapper>
    </FormContainer>
  );
};

export default FormHome;
