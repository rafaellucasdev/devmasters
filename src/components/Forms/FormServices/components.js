import styled from "styled-components";

export const FormContainer = styled.div`
  background: #ececec;
`;

export const FormWrapper = styled.div`
  background: #ececec;
  display: grid;
  grid-auto-columns: minmax(auto, 1fr);
  align-items: center;
  z-index: 1;
  height: 1000px;
  width: 100%;
  max-width: 1100px;
  margin-right: auto;
  margin-left: auto;
  padding: 0 24px;
  justify-content: center;
`;

export const FormCard = styled.form`
  background: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  grid-gap: 16px;
  padding: 40px 60px;
  border-radius: 10px;
  max-width: 1100px;
  margin-top: -30rem;
  border: 1px solid black;
  z-index: 1;

  @media screen and (max-width: 768px) {
    padding: 20px 20px;
    margin-top: -20rem;
  }
`;

export const InputContainer = styled.div`
  align-items: left;
  justify-content: center;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const Label = styled.label`
  font-size: 15px;
  margin-top: 5px;
  color: #031d44;
`;

export const LabelTextArea = styled.textarea`
  font-size: 14px;
  color: #031d44;
  height: 50px;
`;

export const FormTitle = styled.h1`
  color: #031d44;
  font-family: "Exo 2", sans-serif;
  text-align: center;
  font-size: 28px;
  line-height: 1.1;
  font-weight: 600;
`;

export const Img = styled.img`
  width: 100%;
  margin: 0 0 10px 0;
  padding-right: 0;
`;

export const Input = styled.input`
  padding: 15px;
  border-radius: 5px;
  border: 1px solid gray;
  width: 100%;

  &:invalid[focused="true"] {
    border: 1px solid red;
  }

  &:invalid[focused="true"] ~ Error {
    display: block;
  }
`;

export const FormButton = styled.button`
  width: 100px;
  height: 50px;
  padding: 10px;
  border: none;
  background: #031d44;
  color: white;
  border-radius: 5px;
  font-weight: bold;
  font-size: 18px;
  cursor: pointer;
  margin-top: 15px;
  margin-bottom: 30px;

  &:hover {
    background: #ececec;
    color: #031d44;
    transition: all 0.2s ease-in-out;
  }
`;

export const Error = styled.span`
  font-size: 12px;
  padding: 3px;
  color: red;
  display: none;
`;

export const FormInfo = styled.div`
  max-width: 500px;
`;

export const InfoTitle = styled.h1`
  font-family: "Exo 2", sans-serif;
  margin-bottom: 24px;
  font-size: 48px;
  line-height: 1.1;
  font-weight: 600;

  color: ${({ lightText }) => (lightText ? "#fff" : "#010606")};

  @media screen and (max-width: 480px) {
    font-size: 32px;
  }
`;

export const InfoDescription = styled.p`
  font-family: "Exo 2", sans-serif;

  max-width: 440px;
  margin-bottom: 35px;
  font-size: 18px;
  line-height: 24px;

  color: ${({ darkText }) => (darkText ? "#010606" : "#fff")};
`;
