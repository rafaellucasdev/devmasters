import React, { useState } from "react";
import {
  FormContainer,
  FormWrapper,
  FormCard,
  FormButton,
} from "./components";
import FormInput from "./FormInput";

const FormHome = () => {
  const [values, setValues] = useState({
    name: "",
    phone: "",
    email: "",
    menssage: "",
  });

  const inputs = [
    {
      id: 1,
      name: "",
      type: "text",
      placeholder: "",
      errorMessage: "Você deve preencher este campo!",
      label: "Qual o seu ramo de atividade?",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 2,
      name: "",
      type: "text",
      placeholder: "",
      errorMessage: "Você deve preencher este campo!",
      label: "Qual o tamanho do meu negócio hoje?",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 3,
      name: "",
      type: "text",
      placeholder: "",
      errorMessage: "Você deve preencher este campo!",
      label: "O que ofereço ao meu cliente que me diferencia da concorrência?",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 4,
      name: "",
      type: "text",
      placeholder: "",
      errorMessage: "Você deve preencher este campo!",
      label: "Que experiência meu serviço oferece ao meu cliente hoje?",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 5,
      name: "",
      type: "text",
      placeholder: "",
      errorMessage: "Você deve preencher este campo!",
      label: "Quais são os pontos fortes do meu negócio?",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 6,
      name: "",
      type: "text",
      placeholder: "",
      errorMessage: "Você deve preencher este campo!",
      label: "Onde posso melhorar na prestação do meu serviço?",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 7,
      name: "",
      type: "text",
      placeholder: "",
      errorMessage: "Você deve preencher este campo!",
      label: "Que tipo de clientes que já compraram meu produto?",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
    {
      id: 8,
      name: "",
      type: "text",
      placeholder: "",
      errorMessage: "Você deve preencher este campo!",
      label: "Que tipo de cliente eu gostaria de ter?",
      pattern: "^[A-Za-z0-9]{3,16}$",
      required: true,
    },
  ];

  const handleSubmit = (e) => {
    e.preventDefault();
  };

  const onChange = (e) => {
    setValues({ ...values, [e.target.name]: e.target.value });
  };

  return (
    <FormContainer>
      <FormWrapper id="contato">
        <FormCard onSubmit={handleSubmit}>
          {inputs.map((input) => (
            <FormInput
              key={input.id}
              {...input}
              value={values[inputs.name]}
              onChange={onChange}
            />
          ))}
          <FormButton>Enviar</FormButton>
        </FormCard>
      </FormWrapper>
    </FormContainer>
  );
};

export default FormHome;
