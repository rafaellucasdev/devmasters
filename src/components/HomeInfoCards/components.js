import styled from "styled-components";
import { HiChevronRight } from "react-icons/hi";

export const CardsContainer = styled.div`
  height: 800px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #031d44;

  @media screen and (max-width: 1000px) {
    height: 1300px;
  }

  @media screen and (max-width: 768px) {
    height: 1300px;
  }

  @media screen and (max-width: 480px) {
    height: 1600px;
  }
`;

export const InfoWrapper = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  align-items: center;
  justify-content: center;
  grid-gap: 16px;
  padding: 0 50px;

  @media screen and (max-width: 1000px) {
    grid-template-columns: 1fr 1fr;
  }

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
    padding: 0 20px;
  }
`;

export const InfoCard = styled.div`
  background: #ececec;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border-radius: 10px;
  height: 380px;
  padding: 20px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
  transition: all 0.2s ease-in-out;

  &:hover {
    transform: scale(1.02);
    transition: all 0.2 ease-in-out;
    cursor: pointer;
  }
`;

export const InfoIcon = styled.img`
  height: 180px;
  width: 180px;
  margin-bottom: 10px;
`;

export const InfoH1 = styled.h1`
  font-family: "Ubuntu", sans-serif;
  margin-bottom: 64px;
  font-size: 48px;
  line-height: 1.1;
  font-weight: 600;
  color: #fff;
  text-align: center;

  @media screen and (max-width: 480px) {
    font-size: 2rem;
    text-align: center;
  }
`;

export const InfoH2 = styled.h2`
  font-family: "Ubuntu", sans-serif;
  font-size: 1rem;
  margin-bottom: 10px;
  color: #031d44;
  text-align: center;
`;

export const InfoP = styled.p`
  font-family: "Exo 2", sans-serif;
  font-size: 0.9rem;
  text-align: center;
`;

export const ArrowIcon = styled(HiChevronRight)`
  margin-left: 8px;
  font-size: 20px;
`;

export const BtnWrap = styled.div`
  display: flex;
  justify-content: flex-start;
  padding: 40px;
`;
