import React, { useState } from "react";
import Icon1 from "../../images/07.png";
import Icon2 from "../../images/05.png";
import Icon3 from "../../images/06.png";
import { Button } from "../ButtonComponentRouter";

import {
  CardsContainer,
  InfoH1,
  InfoH2,
  InfoWrapper,
  InfoCard,
  InfoIcon,
  InfoP,
  ArrowIcon,
  BtnWrap,
} from "./components";

const HomeInfoTeam = () => {
  const [hover, setHover] = useState(false);
  const onHover = () => {
    setHover(!hover);
  };
  return (
    <>
      <CardsContainer>
        <InfoH1>Porque escolher a DevMasters?</InfoH1>
        <InfoWrapper>
          <InfoCard>
            <InfoIcon src={Icon1} />
            <InfoH2>Gestão eficiente e inovadora</InfoH2>
            <InfoP>
              Nossa equipe é gerida por quem entende do mundo moderno e está
              disposta a evoluir em conjunto.
            </InfoP>
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Icon2} />
            <InfoH2>Qualidade em engenharia de software</InfoH2>
            <InfoP>
              Além do código, enxergamos o valor que o nosso produto agrega, e
              isso é refletido em nossos sistemas.
            </InfoP>
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Icon3} />
            <InfoH2>Redução de custo & Time-to-market</InfoH2>
            <InfoP>
              Economize tempo e dinheiro. Delegue tarefas para profissionais
              especializados, e durma tranquilo sabendo que temos a melhor
              solução para sua empresa.
            </InfoP>
          </InfoCard>
        </InfoWrapper>
        <BtnWrap>
          <Button
            to="/servicos"
            onMouseEnter={onHover}
            onMouseLeave={onHover}
            primary="true"
            dark="false"
          >
            Confira todos os nossos serviços {<ArrowIcon />}
          </Button>
        </BtnWrap>
      </CardsContainer>
    </>
  );
};

export default HomeInfoTeam;
