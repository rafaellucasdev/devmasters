export const homeObjectCompany = {
  id: "marketing",
  lightBg: true,
  lightText: true,
  lightTextDescription: false,
  topLine: "",
  headline: "Marketing e Social Media",
  description: `Quando falamos de Marketing Digital é fundamental uma atenção especial para as campanhas publicitárias. Além de imagens "bonitinhas" nas redes sociais, utilizamos as melhores técnicas, fazemos constantes pesquisas e levantamentos de métricas para criar campanhas assertivas visando maximizar os ganhos e minimizar o investimento.`,
  buttonLabel: "Fale com um especialista",
  imgStart: false,
  img: require("../../images/02.png"),
  alt: "developer",
  dark: true,
  primary: true,
  darkText: false,
  href: "https://api.whatsapp.com/send?phone=5561995880664&text=Ol%C3%A1!%20Vim%20pelo%20site%20e%20gostaria%20de%20tirar%20uma%20d%C3%BAvida."
};
