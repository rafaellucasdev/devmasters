export const homeObjectEnglishClass = {
  id: "consultoria",
  lightBg: true,
  lightText: false,
  lightTextDescription: false,
  topLine: "",
  headline: "Consultoria",
  description:
    "Consultoria em Tecnologia: Nosso time de consultores traz todas as informações e melhores estratégias para sua empresa. Tem uma dúvida sobre tecnologia? Precisa saber sobre as tendências, novos sistemas e aplicações que podem mudar o rumo do seu negócio? Vamos ajudar a inserir sua empresa no mundo digital. Conte com a gente! ",
  buttonLabel: "Fale com um especialista",
  imgStart: false,
  img: require("../../images/03.png"),
  alt: "developer",
  dark: true,
  primary: true,
  darkText: true,
};
