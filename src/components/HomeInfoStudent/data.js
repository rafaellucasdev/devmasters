export const homeObjectStudent = {
  id: "desenvolvimento",
  lightBg: true,
  lightText: false,
  lightTextDescription: false,
  topLine: "",
  headline: "Desenvolvimento de Sistemas customizáveis",
  description:
    "Desenvolvemos soluções exclusivas para sua empresa! Trabalhamos com sistemas web, desktop, aplicativos para dispositivos móveis, infraestrutura de dados, business intelligence, Big Data Analytics e todos os recursos mais atuais da tecnologia, para que sua empresa entregue o maior desempenho, trazendo economia e destaque para seu negócio.",
  buttonLabel: "Fale com um especialista",
  imgStart: false,
  img: require("../../images/01.png"),
  alt: "developer",
  dark: true,
  primary: true,
  darkText: true,
};
