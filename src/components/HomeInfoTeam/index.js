import React, { useState } from "react";
import Image01 from "../../images/team/DANILO_OK.jpg";
import Image02 from "../../images/team/ELIAS_OK.jpg";
import Image03 from "../../images/team/GABRIEL_OK.jpg";
import Image04 from "../../images/team/RAFAEL_ok.jpg";
import Image05 from "../../images/team/SAMUEL_OK.jpg";
import { Button } from "../ButtonComponentRouter";

import {
  CardsContainer,
  InfoH1,
  InfoH2,
  InfoWrapper,
  InfoCard,
  InfoIcon,
  InfoP,
  ArrowIcon,
  BtnWrap,
} from "./components";

const HomeInfoCards = () => {
  const [hover, setHover] = useState(false);
  const onHover = () => {
    setHover(!hover);
  };
  return (
    <>
      <CardsContainer>
        <InfoH1>Nosso time</InfoH1>
        <InfoWrapper>
          <InfoCard>
            <InfoIcon src={Image01} />
            {/* <InfoH2>Gestão eficiente e inovadora</InfoH2>
            <InfoP>
              Nossa equipe é gerida por quem entende do mundo moderno e está
              disposta a evoluir em conjunto.
            </InfoP> */}
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Image02} />
            {/* <InfoH2>Qualidade em engenharia de software</InfoH2>
            <InfoP>
              Além do código, enxergamos o valor que o nosso produto agrega, e
              isso é refletido em nossos sistemas.
            </InfoP> */}
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Image03} />
            {/* <InfoH2>Redução de custo & Time-to-market</InfoH2>
            <InfoP>
              Economize tempo e dinheiro. Delegue tarefas para profissionais
              especializados, e durma tranquilo sabendo que temos a melhor
              solução para sua empresa.
            </InfoP> */}
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Image04} />
            {/* <InfoH2>Redução de custo & Time-to-market</InfoH2>
            <InfoP>
              Economize tempo e dinheiro. Delegue tarefas para profissionais
              especializados, e durma tranquilo sabendo que temos a melhor
              solução para sua empresa.
            </InfoP> */}
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Image05} />
            {/* <InfoH2>Redução de custo & Time-to-market</InfoH2>
            <InfoP>
              Economize tempo e dinheiro. Delegue tarefas para profissionais
              especializados, e durma tranquilo sabendo que temos a melhor
              solução para sua empresa.
            </InfoP> */}
          </InfoCard>
        </InfoWrapper>
        <BtnWrap>
          <Button
            to="/servicos"
            onMouseEnter={onHover}
            onMouseLeave={onHover}
            primary="true"
            dark="false"
          >
            Confira todos os nossos serviços {<ArrowIcon />}
          </Button>
        </BtnWrap>
      </CardsContainer>
    </>
  );
};

export default HomeInfoCards;
