import styled from "styled-components";
import {
  HiChevronDoubleRight,
  HiOfficeBuilding,
  HiCode,
  HiThumbUp,
  HiChat,
  HiCheck,
  HiMail,
  HiPhone,
} from "react-icons/hi";

export const HomeContainer = styled.div`
  background: #0c0c0c;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 30px;
  height: 800px;
  position: relative;
  z-index: 1;

  :before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: linear-gradient(
        180deg,
        rgba(0, 0, 0, 0.2) 0%,
        rgba(0, 0, 0, 0.6) 100%
      ),
      linear-gradient(180deg, rgba(0, 0, 0, 0.2) 0%, transparent 100%);
    z-index: 2;
  }
`;

export const HomeContainerTwo = styled.div`
  background: #ececec;
  display: flex;
  justify-content: center;
  align-items: center;
  /* padding: 0 30px; */
  height: 200px;
  position: relative;
  z-index: 1;

  @media screen and (max-width: 1000px) {
    height: 1600px;
  }

  /* @media screen and (max-width: 1000px) {
    height: 1600px;
  } */
`;

export const HomeBg = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  overflow: hidden;
`;

export const VideoBg = styled.video`
  width: 100%;
  height: 100%;
  -o-object-fit: cover;
  object-fit: cover;
  background: #232a34;
`;

export const HomeBtnMain = styled.div`
  margin-top: 32px;
  display: flex;
  flex-direction: center;
  align-items: center;

  @media screen and (max-width: 760px) {
    display: block;
  }
`;

export const AcademicCap = styled(HiChevronDoubleRight)`
  margin-left: 8px;
  font-size: 20px;
`;

export const OfficeCap = styled(HiOfficeBuilding)`
  margin-left: 8px;
  font-size: 20px;
`;

export const CodeIcon = styled(HiCode)`
  font-size: 30px;
  color: #031d44;
`;

export const LikeIcon = styled(HiThumbUp)`
  font-size: 30px;
  color: #031d44;
`;

export const ChatIcon = styled(HiChat)`
  font-size: 30px;
  color: #031d44;
`;

export const CheckIcon = styled(HiCheck)`
  margin-right: 6px;
  font-size: 20px;
  color: #031d44;
  display: flex;
`;

export const IconMail = styled(HiMail)`
  font-size: 25px;
  margin-right: 15px;
`;

export const IconFone = styled(HiPhone)`
  font-size: 25px;
  margin-right: 15px;
`;

export const InfoWrapper = styled.div`
  max-width: 1000px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  align-items: center;
  justify-content: center;
  grid-gap: 16px;
  padding: 0 50px;

  @media screen and (max-width: 412px) {
    grid-template-columns: 1fr;
    padding: 0 2px;
    height: 1600px;
    max-width: 1200px;
  }

  @media screen and (max-width: 1000px) {
    grid-template-columns: 1fr;
  }

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
    padding: 0 20px;
  }
`;

export const InfoCard = styled.div`
  background: #ececec;
  margin-bottom: 11rem;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  border-radius: 10px;
  height: 500px;
  padding: 20px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.6);
  transition: all 0.2s ease-in-out;
  border: 5px solid #ececec;
  animation-name: fade;
  animation-duration: 800ms;

  @keyframes fade {
    from {
      opacity: 0;
      transform: scale(0.9);
    }
    to {
      opacity: 1;
      transform: scale(1);
    }
  }

  &:hover {
    transform: scale(1.02);
    transition: all 0.2 ease-in-out;
    /* border: 5px solid #e53944; */
    cursor: pointer;
  }

  /* @media screen and (max-width: 360px) {
    width: 300px;
  }

  @media screen and (max-width: 412px) {
    width: 350px;
  } */
`;

export const InfoIcon = styled.img`
  height: 180px;
  width: 180px;
  margin-bottom: 10px;
`;

export const InfoH1 = styled.h1`
  font-family: "Ubuntu", sans-serif;
  /* margin-bottom: 64px; */
  font-size: 28px !important;
  line-height: 1.1;
  font-weight: 800;
  color: #031d44 !important;

  @media screen and (max-width: 480px) {
    font-size: 2rem;
    text-align: center;
  }
`;

export const InfoH2 = styled.h2`
  font-family: "Ubuntu", sans-serif;
  font-size: 1rem;
  margin-bottom: 10px;
  color: #031d44;
  text-align: center;
`;

export const InfoP = styled.p`
  font-size: 15px !important;
  text-align: left !important;
  display: flex;
  color: gray !important;
`;
