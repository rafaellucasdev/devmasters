import React, { useState } from "react";
import Video from "../../videos/video7.mp4";
import { ButtonS } from "../ButtonComponent";
import {
  HomeContainer,
  HomeContainerTwo,
  HomeBg,
  VideoBg,
  AcademicCap,
  InfoWrapper,
  InfoCard,
  InfoH1,
  InfoP,
  CodeIcon,
  LikeIcon,
  ChatIcon,
  CheckIcon,
} from "./components";

const HomeSection = () => {
  const [hover, setHover] = useState(false);
  const onHover = () => {
    setHover(!hover);
  };

  return (
    <>
      <HomeContainer id="inicio">
        <HomeBg>
          <VideoBg autoPlay loop muted src={Video} type="video/mp4" />
        </HomeBg>
        <section className="HomeSectionMain">
          <h1>Sua ideia acontece!</h1>
          {/* <p>
            Nossa característica é trabalhar com praticidade, eficiência e
            transparência!
          </p> */}
          <p>
          A DevMasters desenvolve soluções em software, marketing e tudo que vai do papel ao digital.
          </p>
        </section>
      </HomeContainer>
      <HomeContainerTwo id="inicio">
        <section className="HomeSectionMain">
          <InfoWrapper>
            <InfoCard>
              <CodeIcon />
              <InfoH1>Desenvolvimento de software</InfoH1>
              <InfoP>
                {<CheckIcon />}
                Sistemas totalmente sob medida
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Tecnologias consolidadas no mercado
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Manutenção e revisão constante
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Softwares seguros e eficientes
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Metodologias ágeis para entrega rápida
              </InfoP>
              <ButtonS
                to="desenvolvimento"
                activeStyle
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
                onMouseEnter={onHover}
                onMouseLeave={onHover}
                primary="true"
                dark="true"
              >
                Saiba mais {hover ? <AcademicCap /> : <AcademicCap />}
              </ButtonS>
            </InfoCard>
            <InfoCard>
              <LikeIcon />
              <InfoH1>Serviços de social media</InfoH1>
              <InfoP>
                {<CheckIcon />}
                Gestão e gerenciamento completo de suas redes
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Anúncios das mídias sociais e campanhas de marketing
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Briefing, planejamento e estudo de público
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Sites desenvolvidos de forma 100% personalizada
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Relatórios detalhados de performance
              </InfoP>

              <ButtonS
                to="marketing"
                activeStyle
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
                onMouseEnter={onHover}
                onMouseLeave={onHover}
                primary="true"
                dark="true"
              >
                Saiba mais {hover ? <AcademicCap /> : <AcademicCap />}
              </ButtonS>
            </InfoCard>
            <InfoCard>
              <ChatIcon />
              <InfoH1>Consultoria personalizada</InfoH1>
              <InfoP>
                {<CheckIcon />}
                Dúvidas sobre público e ações de marketing
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Planejamento para quem quer dar o primeiro passo
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Sugestões de conteúdos relevantes para seu negócio
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Otimização de processos da sua empresa
              </InfoP>
              <InfoP>
                {<CheckIcon />}
                Direcionamento para gestão de equipes
              </InfoP>
              <ButtonS
                to="consultoria"
                activeStyle
                spy={true}
                smooth={true}
                offset={-70}
                duration={500}
                onMouseEnter={onHover}
                onMouseLeave={onHover}
                primary="true"
                dark="true"
              >
                Saiba mais {hover ? <AcademicCap /> : <AcademicCap />}
              </ButtonS>
            </InfoCard>
          </InfoWrapper>
        </section>
      </HomeContainerTwo>
    </>
  );
};

export default HomeSection;
