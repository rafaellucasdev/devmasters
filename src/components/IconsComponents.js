import styled from "styled-components";
import { HiMailOpen } from "react-icons/hi";

export const emailIcon = styled(HiMailOpen)`
  margin-left: 8px;
  font-size: 20px;
  color: #000;
`;
