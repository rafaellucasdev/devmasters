import styled from "styled-components";

export const LoaderWP = styled.div`
  opacity: 1;
  top: 0;
  transition: opacity 0.5s linear;
  width: 100%;
  height: 100vh; /* Note a medida */
  background-size: cover;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1 !important;
`;

export const LoaderIcon = styled.img``;

export const LoaderP = styled.p`
  color: #031d44;
`;
