import React from "react";
import { LoaderWP, LoaderIcon } from "./components";
import IconLoader from "./../../images/spinner2.svg";

const Loader = () => {
  return (
    <LoaderWP>
      <LoaderIcon src={IconLoader} />
    </LoaderWP>
  );
};

export default Loader;
