import styled from "styled-components";

// import { LinkS, animateScroll as scroll } from "react-scroll";
import { Link as LinkR } from "react-router-dom";
import { Link as LinkS } from "react-scroll";
import { FaBars } from "react-icons/fa";
import { HiChevronDoubleRight } from "react-icons/hi";

export const Nav = styled.nav`
  background: #031d44;
  height: 100px;
  display: flex;
  justify-content: space-between;
  padding: 0.5rem calc((100vw - 1000px) / 2);
  z-index: 10;
  position: fixed;
  top: 0;
  width: 100%;
  box-shadow: 10px 0px 2px black;
  scroll-behavior: smooth;
`;

export const NavLink = styled(LinkS)`
  font-family: "Ubuntu", sans-serif;
  font-weight: 600;
  color: #fff;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;
  scroll-behavior: smooth;

  // &.active {
  //   border-bottom: 6px solid #01bf71;
  // }

  &:hover {
    color: #e53944;
    transition: all 0.2s ease-in-out;
  }
`;

export const NavLinkR = styled(LinkR)`
  font-family: "Ubuntu", sans-serif;
  font-weight: 600;
  color: #fff;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;
  scroll-behavior: smooth;

  // &.active {
  //   border-bottom: 6px solid #01bf71;
  // }

  &:hover {
    color: #e53944;
    transition: all 0.2s ease-in-out;
  }
`;

export const Bars = styled(FaBars)`
  display: none;
  color: #fff;

  @media screen and (max-width: 760px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

export const NavMenu = styled.div`
  display: flex;
  align-items: center;
  margin-right: -24px;

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-right: 24px;

  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavRouteLink = styled(LinkR)`
  font-family: "Exo 2", sans-serif;
  border-radius: 50px;
  display: flex;
  justify-content: center;
  background: #e53944;
  padding: 10px 22px;
  color: #fff;
  border: none;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: #fff;
    color: #010606;
  }
`;

export const NavRouteLinkA = styled.a`
  font-family: "Exo 2", sans-serif;
  border-radius: 50px;
  display: flex;
  justify-content: center;
  background: #e53944;
  padding: 10px 22px;
  color: #fff;
  border: none;
  outline: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;

  &:hover {
    transition: all 0.2s ease-in-out;
    background: #fff;
    color: #010606;
  }
`;

export const IconArrow = styled(HiChevronDoubleRight)`
  margin-left: 8px;
  font-size: 20px;
`;
