import { Nav, NavLink, Bars, NavMenu, NavBtn, NavRouteLinkA, NavLinkR } from "./components";
import { animateScroll as scroll } from "react-scroll";
import { scroller } from "react-scroll";

const NavBar = ({ toggle }) => {

  const toggleHome = () => {
    scroll.scrollToTop();
  }

  const toggleAbout = () => {
    scroller.scrollTo(500, {
      duration: 1500,
      delay: 100,
      smooth: true,
      containerId: 'IdSobre',
      offset: 50,
    });
  }

  return (
    <header>
      <Nav>
        <NavLinkR to="/">
          <img
            style={{ height: "100%" }}
            src={require("../../assets/img/devmasters.png")}
            alt="logo"
            to="/"
            activeStyle
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
            onClick={toggleHome}
          />
        </NavLinkR>
        <Bars onClick={toggle} />
        <NavMenu>
          <NavLinkR
            to="/"
            spy={true}
            smooth={true}
            offset={-80}
            exact={true}
            duration={500}
            onClick={toggleHome}
          >
            Início
          </NavLinkR>
          <NavLink
            to="IdSobre"
            spy={true}
            smooth={true}
            offset={-80}
            exact={true}
            duration={500}
            onClick={toggleAbout}
          >
            Sobre
          </NavLink>
          <NavLink
            to="contato"
            activeStyle
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
          >
            Contato
          </NavLink>
          <NavLinkR
            to="servicos"
          >
            Serviços
          </NavLinkR>
        </NavMenu>
        <NavBtn>
          <NavRouteLinkA
            href="https://api.whatsapp.com/send?phone=5561995880664&text=Ol%C3%A1!%20Vim%20pelo%20site%20e%20gostaria%20de%20tirar%20uma%20d%C3%BAvida."
          >
            Solicite um orçamento
          </NavRouteLinkA>
        </NavBtn>
      </Nav>
    </header>
  );
};

export default NavBar;
