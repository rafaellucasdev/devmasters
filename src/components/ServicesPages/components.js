import styled from "styled-components";
import imagem from "../../images/image13.jpg";

export const ServicesContainer = styled.div`
  background: linear-gradient(rgba(0, 0, 0, 0.596), rgba(0, 0, 0, 0.7)),
    url(${imagem});
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 30px;
  height: 550px;
  width: auto;
  position: relative;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

  @media screen and (max-width: 600px) {
    height: 650px;
  }
`;

export const ServicesWrapper = styled.div`
  max-width: 1100px;
  margin: 0 auto;
  display: grid;
  grid-template-columns: 1fr;
  align-items: center;
  justify-content: center;
  grid-gap: 16px;
  padding: 0 50px;
  animation-name: fade;
  animation-duration: 800ms;

  @keyframes fade {
    from {
      opacity: 0;
      transform: scale(0.9);
    }
    to {
      opacity: 1;
      transform: scale(1);
    }
  }

  @media screen and (max-width: 968px) {
    grid-template-columns: 1fr 1fr;
  }

  @media screen and (max-width: 768px) {
    grid-template-columns: 1fr;
    padding: 0 20px;
  }
`;

export const ServicesTitle = styled.h1`
  font-family: "Ubuntu", sans-serif;
  margin-bottom: 24px;
  font-size: 48px;
  line-height: 1.1;
  font-weight: 600;
  color: #fff;

  @media screen and (max-width: 480px) {
    font-size: 2rem;
    text-align: center;
  }
`;

export const ServicesDescription = styled.p`
  font-family: "Exo 2", sans-serif;
  max-width: 1100px;
  margin-bottom: 16px;
  font-size: 18px;
  line-height: 24px;
  color: #fff;
`;
