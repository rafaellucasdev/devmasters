import React from "react";
import {
  ServicesContainer,
  ServicesWrapper,
  ServicesTitle,
  ServicesDescription,
} from "./components";
import FormHome from "../Forms/FormServices";

const ServicesPages = () => {
  return (
    <>
      <ServicesContainer>
        <ServicesWrapper>
          <ServicesTitle>Vamos nos conhecer um pouco!</ServicesTitle>
          <ServicesDescription>
            Antes de termos um contato mais direto, precisamos que você preencha
            apenas algumas perguntas, somente para entendermos o seu perfil.
          </ServicesDescription>
        </ServicesWrapper>
      </ServicesContainer>
      <FormHome />
    </>
  );
};

export default ServicesPages;
