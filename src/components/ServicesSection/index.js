import React, { useState } from "react";
import { Button } from "../ButtonComponent";
import {
  ServicesContainer,
  ServicesSectionWrapper,
  ServicesH1,
  ServicesP,
  ServicesBtnMain,
  IconArrow,
} from "./components";
// import imageBackgound from "./../../images/image01.jpg";

const ServicesSection = () => {
  const [hover, setHover] = useState(false);
  const onHover = () => {
    setHover(!hover);
  };
  return (
    <ServicesContainer id="servicos">
      <ServicesSectionWrapper>
        <ServicesH1>Nosso foco de atuação</ServicesH1>
        <ServicesP>
          Temos a tecnologia como combustível para nosso negócio. Trazendo a
          vanguarda do mundo digital, oferecemos soluções baseadas em três
          pilares: Desenvolvimento de software, consultorias tecnológicas e
          Marketing digital. Cada área de atuação tem uma equipe especializada
          com o foco total em resolver problemas de forma eficiente, utilizando
          métodos ágeis e tudo que existe de mais moderno.
        </ServicesP>
        <ServicesBtnMain>
          <Button
            to="/"
            onMouseEnter={onHover}
            onMouseLeave={onHover}
            primary="true"
            dark="true"
          >
            Agende uma reunião {hover ? <IconArrow /> : <IconArrow />}
          </Button>
        </ServicesBtnMain>
      </ServicesSectionWrapper>
    </ServicesContainer>
  );
};

export default ServicesSection;
