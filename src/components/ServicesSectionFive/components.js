import styled from "styled-components";
import imagem from "./../../images/image11.jpg";
import { HiChevronDoubleRight } from "react-icons/hi";

export const ServicesContainer = styled.div`
  background: linear-gradient(rgba(0, 0, 0, 0.596), rgba(0, 0, 0, 0.7)),
    url(${imagem});
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 30px;
  height: 800px;
  width: auto;
  position: relative;
  /* background-image: url(${imagem}); */
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
`;

export const ServicesSectionWrapper = styled.div`
  /* display: grid;
  z-index: 1;
  height: 800px;
  width: 100%;
  max-width: 1100px;
  margin-right: auto;
  margin-left: auto;
  padding: 0 24px;
  justify-content: center; */
`;

// export const AboutBg = styled.div`
//   position: absolute;
//   top: 0;
//   right: 0;
//   bottom: 0;
//   left: 0;
//   width: 100%;
//   height: 100%;
//   overflow: hidden;
// `;

export const ServicesBtnMain = styled.div`
  margin-top: 32px;
  display: flex;
  flex-direction: center;
  align-items: center;

  @media screen and (max-width: 760px) {
    display: block;
  }
`;

export const ImgBg = styled.img`
  width: 100%;
  height: 100%;
  -o-object-fit: cover;
  object-fit: cover;
`;

export const ServicesH1 = styled.h1`
  font-family: "Ubuntu", sans-serif;
  margin-bottom: 24px;
  max-width: 555px;
  font-size: 48px;
  line-height: 1.1;
  font-weight: 600;
  color: #fff;
  /* color: ${({ lightText }) => (lightText ? "#f7f8fa" : "#031d44")}; */

  @media screen and (max-width: 480px) {
    font-size: 32px;
  }
`;

export const ServicesP = styled.p`
  font-family: "Exo 2", sans-serif;
  max-width: 1100px;
  margin-bottom: 35px;
  font-size: 18px;
  line-height: 24px;
  color: #fff;
  /* color: ${({ darkText }) => (darkText ? "#fff" : "#cdcdcd")}; */
`;

export const IconArrow = styled(HiChevronDoubleRight)`
  margin-left: 8px;
  font-size: 20px;
`;
