import React, { useState } from "react";
import { Button } from "../ButtonComponent";
import {
  ServicesContainer,
  ServicesSectionWrapper,
  ServicesH1,
  ServicesP,
  ServicesBtnMain,
  IconArrow,
} from "./components";
// import imageBackgound from "./../../images/image01.jpg";

const ServicesSectionFive = () => {
  const [hover, setHover] = useState(false);
  const onHover = () => {
    setHover(!hover);
  };
  return (
    <ServicesContainer>
      <ServicesSectionWrapper>
        <ServicesH1>
          Podemos preparar uma proposta personalizada para você!
        </ServicesH1>
        <ServicesP>
          Fale com um de nossos especialistas para termos um direcionamento e
          personalizar uma proposta para você. Estamos sempre dispostos a
          entender o seu problema e elaborar a melhor solução.
        </ServicesP>
        <ServicesBtnMain>
          <Button
            to="/"
            onMouseEnter={onHover}
            onMouseLeave={onHover}
            primary="true"
            dark="true"
          >
            Agende uma reunião {hover ? <IconArrow /> : <IconArrow />}
          </Button>
        </ServicesBtnMain>
      </ServicesSectionWrapper>
    </ServicesContainer>
  );
};

export default ServicesSectionFive;
