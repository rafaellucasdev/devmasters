import React, { useState } from "react";
import Icon1 from "../../images/icons/ideias.png";
import Icon2 from "../../images/icons/estrategia-de-negocio.png";
import Icon3 from "../../images/icons/parceria.png";
import Icon4 from "../../images/icons/processo-interno.png";
import Icon5 from "../../images/icons/satisfeito.png";
import Icon6 from "../../images/icons/verificado.png";
import { Button } from "../ButtonComponent";

import {
  CardsContainer,
  InfoH1,
  InfoH2,
  InfoWrapper,
  InfoCard,
  InfoIcon,
  ArrowIcon,
  BtnWrap,
} from "./components";

const ServicesSectionInfoCards = () => {
  const [hover, setHover] = useState(false);
  const onHover = () => {
    setHover(!hover);
  };
  return (
    <>
      <CardsContainer id="etapas">
        <InfoH1>Pricipais etapas do nosso trabalho</InfoH1>
        <InfoWrapper>
          <InfoCard>
            <InfoIcon src={Icon1} />
            <InfoH2>Conheçemos sua ideia</InfoH2>
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Icon2} />
            <InfoH2> Apresentamos uma solução </InfoH2>
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Icon3} />
            <InfoH2>Formalizamos nossa parceria </InfoH2>
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Icon4} />
            <InfoH2>Nossa equipe desenvolve</InfoH2>
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Icon5} />
            <InfoH2>Você sempre por dentro </InfoH2>
          </InfoCard>
          <InfoCard>
            <InfoIcon src={Icon6} />
            <InfoH2>Entregamos o combinado!</InfoH2>
          </InfoCard>
        </InfoWrapper>
        <BtnWrap>
          <Button
            to="/desenvolvimento-de-software"
            onMouseEnter={onHover}
            onMouseLeave={onHover}
            primary="true"
            dark="false"
          >
            Quero saber mais {<ArrowIcon />}
          </Button>
        </BtnWrap>
      </CardsContainer>
    </>
  );
};

export default ServicesSectionInfoCards;
