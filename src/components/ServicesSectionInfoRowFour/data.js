export const ServicesObjectFour = {
  id: "Sobre",
  lightBg: true,
  lightText: false,
  lightTextDescription: false,
  topLine: "",
  headline: "Criação de Web Sites",
  description:
    "Definimos o objetivo do seu site. Seja como vitrine, facilitando a comunicação ou transmitindo os valores da sua empresa para o seu cliente. Registramos o domínio como ponto inicial, escolhemos a plataforma de hospedagem de acordo com o que seu site irá demandar em desempenho. Elaboramos um conteúdo de qualidade, criamos boas descrições do seu produto ou serviço. Criamos a identidade visual, e planejamos uma estratégia de marketing direcionada ao seu negócio.",
  buttonLabel: "Veja como funciona",
  imgStart: false,
  img: require("../../images/img04.png"),
  alt: "developer",
  dark: true,
  primary: true,
  darkText: true,
};
