import React from "react";
import {
  SectionInfoContainer,
  SectionInfoWrapper,
  SectionInfoRow,
  Column1,
  TextWrapper,
  TopLine,
  Heading,
  Subtitle,
  Column2,
  ImgWrap,
  Img,
} from "./components";

const ServicesSectionInfoRowFour = ({
  lightBg,
  id,
  imgStart,
  topLine,
  lightText,
  headline,
  darkText,
  description,

  img,
  alt,
}) => {
  return (
    <>
      <SectionInfoContainer lightBg={lightBg} id={id}>
        <SectionInfoWrapper>
          <SectionInfoRow imgStart={imgStart}>
            <Column1>
              <ImgWrap>
                <Img src={img} alt={alt} />
              </ImgWrap>
            </Column1>
            <Column2>
              <TextWrapper>
                <TopLine>{topLine}</TopLine>
                <Heading lightText={lightText}>{headline}</Heading>
                <Subtitle darkText={darkText}>{description}</Subtitle>
              </TextWrapper>
            </Column2>
          </SectionInfoRow>
        </SectionInfoWrapper>
      </SectionInfoContainer>
    </>
  );
};

export default ServicesSectionInfoRowFour;
