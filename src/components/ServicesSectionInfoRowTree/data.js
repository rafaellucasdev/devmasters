export const ServicesObjectTree = {
  id: "Sobre",
  lightBg: true,
  lightText: false,
  lightTextDescription: false,
  topLine: "",
  headline: "Gestão de mídias",
  description:
    "Prezamos pela qualidade, por isso entregamos postagens que agreguem valor. Focamos nas plataformas que tragam o maior ROI: Facebook, Twitter, LinkedIn ou Instagram. Estabelecemos um cronograma juntamente ao nosso cliente, seguindo horários estratégicos e gerando engajamento. Sempre presentes, analisamos o comportamento e engajamento do público, e mais que isso, valorizamos a interação e o networking. ",
  buttonLabel: "Veja como funciona",
  imgStart: false,
  img: require("../../images/img03.png"),
  alt: "developer",
  dark: true,
  primary: true,
  darkText: true,
};
