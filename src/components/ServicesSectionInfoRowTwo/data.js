export const ServicesObjectTwo = {
  id: "Sobre",
  lightBg: true,
  lightText: false,
  lightTextDescription: false,
  topLine: "",
  headline: "Campanhas de marketing e identidade visual",
  description:
    "Para fazer campanha de marketing efetiva, começamos com o planejamento. Entendemos o por que da campanha, duração e orçamento, então definimos os objetivos. Entendemos o público alvo, definimos a linha de comunicação e os canais a serem utilizados. Por último, desenvolvemos o conteúdo, testamos, monitoramos e otimizamos.",
  buttonLabel: "Veja como funciona",
  imgStart: false,
  img: require("../../images/img02.png"),
  alt: "developer",
  dark: true,
  primary: true,
  darkText: true,
};
