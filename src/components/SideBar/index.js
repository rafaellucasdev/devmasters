import React from "react";
import {
  SidebarContainer,
  Icon,
  CloseIcon,
  SidebarWrapper,
  SidebarMenu,
  SidebarLink,
  SidebarLinkS,
  SideBtnWrap,
  SidebarRoute,
} from "./components";
import { animateScroll as scroll } from "react-scroll";

const SideBar = ({ isOpen, toggle }) => {

  const toggleHome = () => {
    scroll.scrollToTop();
  }

  return (
    <SidebarContainer isOpen={isOpen} onClick={toggle}>
      <Icon onClick={toggle}>
        <CloseIcon />
      </Icon>
      <SidebarWrapper>
        <SidebarMenu>
          <SidebarLink
            to="/"
            spy={true}
            smooth={true}
            offset={-80}
            exact={true}
            duration={500}
            onClick={toggleHome}
          >
            Início
          </SidebarLink>
          <SidebarLinkS
            to="IdSobre"
            activeStyle
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
            onClick={toggle}
          >
            Sobre
          </SidebarLinkS>
          <SidebarLinkS to="contato" activeStyle spy={true}
            smooth={true}
            offset={-70}
            duration={500} onClick={toggle}>
            Contato
          </SidebarLinkS>
          {/* <SidebarLink to="/blog" activeStyle onClick={toggle}>
            Blog
          </SidebarLink> */}
          <SidebarLink
            to="/servicos"
            activeStyle
            spy={true}
            smooth={true}
            offset={-70}
            duration={500}
            onClick={toggle}
          >
            Serviços
          </SidebarLink>
        </SidebarMenu>
        <SideBtnWrap>
          <SidebarRoute to="/desenvolvimento-de-software" activeStyle>
            Solicite um orçamento
          </SidebarRoute>
        </SideBtnWrap>
      </SidebarWrapper>
    </SidebarContainer>
  );
};

export default SideBar;
