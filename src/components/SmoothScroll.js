import styled from "styled-components";

import { Link, Button, Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll'

export const NavBtnLinkScroll = styled(Link)`
font-family: "Ubuntu", sans-serif;
font-weight: 600;
color: #fff;
display: flex;
align-items: center;
text-decoration: none;
padding: 0 1rem;
height: 100%;
cursor: pointer;
scroll-behavior: smooth;

// &.active {
//   color: #8b8989;
// }

&:hover {
  color: #e53944;
  transition: all 0.2s ease-in-out;
}
`;