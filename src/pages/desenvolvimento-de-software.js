import React from "react";
import ServicesPages from "../components/ServicesPages";
import ScrollToTop from "./../components/ScrollToTop";

const DesenvolvimentoDeSoftware = () => {
  return (
    <>
      <ScrollToTop />
      <ServicesPages />
    </>
  );
};
export default DesenvolvimentoDeSoftware;
