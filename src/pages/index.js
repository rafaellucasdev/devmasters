import React from "react";
import "../App.css";
import ScrollToTop from "./../components/ScrollToTop";
import HomeSection from "../components/HomeSection";
import HomeInfoStudent from "../components/HomeInfoStudent";
import HomeInfoCompany from "../components/HomeInfoCompany";
import HomeInfoEnglishClass from "../components/HomeInfoEnglishClass";
import FormHome from "./../components/Forms/FormHome";
import { homeObjectStudent } from "../components/HomeInfoStudent/data";
import { homeObjectCompany } from "../components/HomeInfoCompany/data";
import { homeObjectEnglishClass } from "../components/HomeInfoEnglishClass/data";
import HomeInfoCards from "../components/HomeInfoCards";
import AboutSection from "../components/AboutSection";
// import HomeInfoTeam from "../components/HomeInfoTeam"

const Home = () => {
  return (
    <>
      <ScrollToTop />
      <HomeSection />
      <HomeInfoStudent {...homeObjectStudent} />
      <HomeInfoCompany {...homeObjectCompany} />
      <HomeInfoEnglishClass {...homeObjectEnglishClass} />
      <AboutSection />
      {/* <HomeInfoTeam /> */}
      <HomeInfoCards />
      <FormHome />
    </>
  );
};

export default Home;
