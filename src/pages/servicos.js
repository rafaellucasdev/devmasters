import React from "react";
import ScrollToTop from "./../components/ScrollToTop";
import ServicesSection from "./../components/ServicesSection";
import ServicesSectionInfoRow from "../components/ServicesSectionInfoRow";
import ServicesSectionInfoRowTwo from "../components/ServicesSectionInfoRowTwo";
import ServicesSectionInfoRowTree from "../components/ServicesSectionInfoRowTree";
import ServicesSectionInfoRowFour from "../components/ServicesSectionInfoRowFour";
import ServicesSectionInfoCards from "../components/ServicesSectionInfoCards";
import ServicesSectionFive from "../components/ServicesSectionFive";
import { ServicesObject } from "../components/ServicesSectionInfoRow/data";
import { ServicesObjectTwo } from "../components/ServicesSectionInfoRowTwo/data";
import { ServicesObjectTree } from "../components/ServicesSectionInfoRowTree/data";
import { ServicesObjectFour } from "../components/ServicesSectionInfoRowFour/data";

const Servicos = () => {
  return (
    <>
      <ScrollToTop />
      <ServicesSection />
      <ServicesSectionInfoRow {...ServicesObject} />
      <ServicesSectionInfoRowTwo {...ServicesObjectTwo} />
      <ServicesSectionInfoRowTree {...ServicesObjectTree} />
      <ServicesSectionInfoRowFour {...ServicesObjectFour} />
      <ServicesSectionInfoCards />
      <ServicesSectionFive />
    </>
  );
};

export default Servicos;
